<?php

namespace AppBundle\Service;

/**
 * Class VideoService
 * @package AppBundle\Service
 */
class VideoService
{
    /**
     * List of enabled video providers
     */
    private $providers = ['Vimeo', 'Youtube', 'Rutube'];

    /**
     * Function alternately creates video providers and trying get videoId.
     * If videoId found, it means regexp matched, and function returns video provider object.
     *
     * @string $data
     * @return mixed
     */
    function getProvider($data)
    {
        if ($data != false) {
            foreach ($this->providers as $provider) {
                $providerObj = 'AppBundle\\Provider\\' . $provider;
                $providerObj = new $providerObj();
                if ($videoId = $providerObj->getVideoId($data)) {
                    return $providerObj;
                }
                unset($providerObj);
            }
        }

        return false;
    }
}