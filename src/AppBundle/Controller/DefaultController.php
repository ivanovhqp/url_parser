<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Video;
use AppBundle\Service\VideoService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $videoProvider = $this->get(VideoService::class)->getProvider(
                $request->request->get('text')
            );

            if ($videoProvider && $parsedVideo = $videoProvider->getVideoData()) {

                $em = $this->getDoctrine()->getManager();

                // Check video in our database, insert if not
                if(!$em->getRepository('AppBundle:Video')->findOneBy(['provider' => $videoProvider->getName(), 'provider_video_id' => $videoProvider->videoId])) {
                    $video = new Video();
                    $video
                        ->setName($parsedVideo['title'])
                        ->setDescription($parsedVideo['description'])
                        ->setPreview($parsedVideo['preview'])
                        ->setIframe($parsedVideo['embed'])
                        ->setProvider($videoProvider->getName())
                        ->setProviderVideoId($videoProvider->videoId)
                    ;
                    $em->persist($video);
                    $em->flush();
                }

            }
        }

        return $this->render('default/index.html.twig', [
            'videos' => $this->getDoctrine()->getRepository('AppBundle:Video')->findAll()
        ]);
    }
}
