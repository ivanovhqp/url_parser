<?php

namespace AppBundle\Provider;

/**
 * Class VideoProvider
 * @package AppBundle\Provider
 */
abstract class VideoProvider
{
    public $host = false; // video provider host for requests
    public $videoId = false;
    public $patterns = []; // patterns for reqexp

    /**
     * Function returns name of Video Provider
     *
     * @return string
     */
    abstract public function getName();

    /**
     * Function makes request, filtering data and return array combined all information about video
     *
     * @return mixed
     */
    abstract public function getVideoData();

    /**
     * Function sort out all provider regexp patterns, and trying find video id
     *
     * @string $data
     * @return mixed
     */
    public function getVideoId($data)
    {
        foreach ($this->patterns as &$pattern) {
            preg_match($pattern, $data, $matches);
            if (!empty($matches[1])) {
                return $this->videoId = $matches[1];
            }
        }

        return false;
    }
}