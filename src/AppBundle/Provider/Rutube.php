<?php

namespace AppBundle\Provider;

class Rutube extends VideoProvider
{
    public function __construct()
    {
        $this->host = 'http://rutube.ru/api/oembed/';
        $this->patterns = [
            '/rutube\.ru\/video\/([\w|\d]+)/'
        ];
    }

    public function getName()
    {
        return 'rutube';
    }

    public function getVideoData()
    {
        if (empty($this->videoId)) {
            throw new Exception('Empty videoId');
        }

        $data = json_decode(
            file_get_contents($this->host . '?url=http://rutube.ru/video/' . $this->videoId . '/&format=json')
        );

        if (!$data) {
            return false;
        }

        return [
            'title' => $data->title,
            'description' => 'empty',
            'preview' => $data->thumbnail_url,
            'embed' => $data->html
        ];
    }
}