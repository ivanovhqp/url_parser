<?php

namespace AppBundle\Provider;

class Youtube extends VideoProvider
{
    public function __construct()
    {
        $this->api_host = 'https://www.googleapis.com/youtube/v3/';
        $this->api_key = 'AIzaSyDIIFdJo62HFCs81VTT7oTiSA_mlZmHTd8';
        $this->video_embed_host = 'https://www.youtube.com/watch?v=';
        $this->patterns = [
            '/youtube\.com\/watch\?v\=([\w|\d|\_|\-]+)/',
            '/youtube\.com\/embed\/([\w|\d|\_|\-]+)/',
            '/youtu\.be\/([\w|\d|\_|\-]+)/'
        ];
    }

    public function getName()
    {
        return 'youtube';
    }

    public function getVideoData()
    {
        if (empty($this->videoId)) {
            throw new Exception('Empty videoId');
        }

        $data = json_decode(
            file_get_contents($this->api_host . 'videos?id=' . $this->videoId . '&key=' . $this->api_key . '&part=snippet')
        );

        if (isset($data->items) && !empty($data->items)) {
            return [
                'title' => $data->items[0]->snippet->title,
                'description' => $data->items[0]->snippet->description,
                'preview' => $data->items[0]->snippet->thumbnails->standard->url,
                'embed' => '<iframe width="490" height="370" src="https://www.youtube.com/embed/'.$this->videoId.'" frameborder="0" allowfullscreen="allowfullscreen" data-link="https://www.youtube.com/watch?v='.$this->videoId.'"></iframe>'
            ];
        }

        return false;
    }
}