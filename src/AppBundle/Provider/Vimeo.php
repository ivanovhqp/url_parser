<?php

namespace AppBundle\Provider;

class Vimeo extends VideoProvider
{
    public function __construct()
    {
        $this->host = 'https://vimeo.com/api/oembed.json';
        $this->patterns = [
            '/vimeo\.com\/(\d+)/',
            '/vimeo\.com\/video\/(\d+)/'
        ];
    }

    public function getName()
    {
        return 'vimeo';
    }

    public function getVideoData()
    {
        if (empty($this->videoId)) {
            throw new Exception('Empty videoId');
        }

        $data = json_decode(
            file_get_contents($this->host . '?url=https%3A//vimeo.com/' . $this->videoId)
        );

        if (!$data) {
            return false;
        }

        return [
            'title' => $data->title,
            'description' => $data->description,
            'preview' => $data->thumbnail_url,
            'embed' => $data->html
        ];
    }
}